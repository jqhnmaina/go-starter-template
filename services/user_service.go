package services

import (
	"context"
	"errors"
	"gopkg.in/guregu/null.v4"
	"net/http"
	"strings"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/entities"
	"bitbucket.org/jqhnmaina/go-starter-template/forms"
	"bitbucket.org/jqhnmaina/go-starter-template/repos"
	"bitbucket.org/jqhnmaina/go-starter-template/utils"
)

type (
	UserService interface {
		CreateUser(context.Context, *db.UserDB, *forms.CreateUserForm) (*entities.User, error)
		GetUserDetails(context.Context, *db.UserDB, int64) (*entities.User, error)
	}

	AppUserService struct {
		userRepository repos.UserRepository
	}
)

func NewUserService(
	userRepository repos.UserRepository,
) UserService {
	return &AppUserService{
		userRepository: userRepository,
	}
}

func (s *AppUserService) CreateUser(
	ctx context.Context,
	userDB *db.UserDB,
	form *forms.CreateUserForm,
) (*entities.User, error) {
	// validate gender
	if !form.Gender.IsValid() {
		return nil, utils.NewErrorWithCodeAndMessage(
			errors.New("invalid gender"),
			http.StatusBadRequest,
			"Invalid gender provided",
			"Gender=[%v] provided is invalid",
			form.Gender,
		)
	}

	// validate name length
	if len(form.Name) > 50 {
		return nil, utils.NewErrorWithCodeAndMessage(
			errors.New("invalid user name"),
			http.StatusBadRequest,
			"User name cannot be more than 50 characters",
			"Name=[%v] provided is more that 50 characters",
			form.Name,
		)
	}

	// check if phone number is available
	phoneNumber := &entities.PhoneNumber{
		CountryCode: form.PhoneNumber.CountryCode,
		Number:      form.PhoneNumber.Number,
	}
	_, err := s.userRepository.FindByPhoneNumber(ctx, userDB, phoneNumber)
	if err != nil && !utils.IsErrNoRows(err) {
		return nil, utils.NewErrorWithCodeAndMessage(
			err,
			http.StatusBadRequest,
			"Failed to valid user phone number",
			"Failed to valid user phone number",
			form.Name,
		)
	}

	if err == nil {
		return nil, utils.NewErrorWithCodeAndMessage(
			errors.New("invalid user phone number provided"),
			http.StatusConflict,
			"User phone number is not available",
			"phone=[%v] provided is not available",
			form.PhoneNumber,
		)
	}

	// if email is provided check if it available
	if len(form.Email) > 2 {
		_, err := s.userRepository.FindByEmail(ctx, userDB, form.Email)
		if err != nil && !utils.IsErrNoRows(err) {
			return nil, utils.NewErrorWithCodeAndMessage(
				errors.New("invalid user email"),
				http.StatusBadRequest,
				"User email cannot be more than 50 characters",
				"Email=[%v] provided is more that 50 characters",
				form.Name,
			)
		}

		if err == nil {
			return nil, utils.NewErrorWithCodeAndMessage(
				errors.New("invalid user email not available"),
				http.StatusConflict,
				"User email is not available",
				"Email=[%v] provided is not available",
				form.PhoneNumber,
			)
		}
	}

	user := &entities.User{
		Name:   strings.TrimSpace(form.Name),
		DOB:    form.DOB,
		Gender: form.Gender,
		PhoneNumber: entities.PhoneNumber{
			CountryCode: form.PhoneNumber.CountryCode,
			Number:      form.PhoneNumber.Number,
		},
	}

	if len(form.Email) > 2 {
		user.Email = null.StringFrom(strings.TrimSpace(form.Email))
	}

	err = s.userRepository.Save(ctx, userDB, user)
	if err != nil {
		return nil, utils.NewErrorWithCodeAndMessage(
			err,
			http.StatusInternalServerError,
			"Failed to save user details",
			"Failed to save user details",
		)
	}

	return user, nil
}

func (s *AppUserService) GetUserDetails(
	ctx context.Context,
	userDB *db.UserDB,
	userID int64,
) (*entities.User, error) {

	user, err := s.userRepository.FindByID(ctx, userDB, userID)

	if err != nil {
		if utils.IsErrNoRows(err) {
			return user, utils.NewErrorWithCodeAndMessage(
				err,
				http.StatusNotFound,
				"User with given id was not found",
				"User with id[%v] was not found",
				userID,
			)
		}

		return user, utils.NewErrorWithCodeAndMessage(
			err,
			http.StatusInternalServerError,
			"Failed to find user with given id",
			"Failed to find user with id[%v]",
			userID,
		)
	}

	return user, nil
}
