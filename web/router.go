package web

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/services"
	"bitbucket.org/jqhnmaina/go-starter-template/web/users"
)

type AppRouter struct {
	*gin.Engine
}

func (router *AppRouter) Run() {
	appPort := os.Getenv("PORT")
	if appPort == "" {
		appPort = "9000"
	}
	router.Engine.Run(fmt.Sprintf("0.0.0.0:%s", appPort))
}

func corsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
	}
}

func BuildRouter(
	userDB *db.UserDB,
	userService services.UserService,
) *AppRouter {
	router := gin.Default()
	router.Use(gin.Recovery())
	router.Use(corsMiddleware())

	userRoutes := router.Group("")
	{
		users.AddEndpoints(userRoutes, userDB, userService)
	}

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"success": false, "message": "Endpoint Not Found"})
	})

	return &AppRouter{router}
}
