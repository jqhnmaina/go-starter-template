package users

import (
	"math/rand"
	"time"

	"github.com/gin-gonic/gin"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/services"
)

func generateRandomNum(min, max int) int {
	return rand.New(rand.NewSource(time.Now().UnixNano())).Intn(max-min) + min
}

func AddEndpoints(
	r *gin.RouterGroup,
	userDB *db.UserDB,
	userService services.UserService,
) {
	r.POST("users", createUser(userDB, userService))
	r.GET("users/:id", getUser(userDB, userService))
	r.GET("random-num", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"random_num": generateRandomNum(20, 10000),
		})
	})
}
