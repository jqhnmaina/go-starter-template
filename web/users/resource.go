package users

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/forms"
	"bitbucket.org/jqhnmaina/go-starter-template/services"
	"bitbucket.org/jqhnmaina/go-starter-template/utils"
)

func createUser(
	userDB *db.UserDB,
	userService services.UserService,
) func(c *gin.Context) {
	return func(c *gin.Context) {
		var form forms.CreateUserForm
		err := c.BindJSON(&form)
		if err != nil {
			appError := utils.NewErrorWithCodeAndMessage(
				err,
				http.StatusBadRequest,
				"Invalid form provided",
				"Failed to bind to create user form",
			)

			appError.LogErrorMessages()

			c.JSON(appError.HttpStatus(), appError.JsonResponse())
			return
		}

		user, err := userService.CreateUser(c.Request.Context(), userDB, &form)
		if err != nil {
			appError := utils.NewError(
				err,
				"Failed to create user",
			)

			appError.LogErrorMessages()

			c.JSON(appError.HttpStatus(), appError.JsonResponse())
			return
		}

		c.JSON(http.StatusCreated, user)
		return
	}
}

func getUser(
	userDB *db.UserDB,
	userService services.UserService,
) func(c *gin.Context) {
	return func(c *gin.Context) {
		userID, err := strconv.ParseInt(c.Param("id"), 10, 64)
		if err != nil {
			appError := utils.NewErrorWithCodeAndMessage(
				err,
				http.StatusBadRequest,
				"Invalid user id provided",
				"Failed to parse userID=[%v]",
				c.Param("id"),
			)

			appError.LogErrorMessages()

			c.JSON(appError.HttpStatus(), appError.JsonResponse())
			return
		}

		user, err := userService.GetUserDetails(c.Request.Context(), userDB, userID)
		if err != nil {
			appError := utils.NewError(
				err,
				"Failed to get user detail for user id=[%v]",
				userID,
			)

			appError.LogErrorMessages()

			c.JSON(appError.HttpStatus(), appError.JsonResponse())
			return
		}

		c.JSON(http.StatusOK, user)
		return
	}
}
