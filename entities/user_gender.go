package entities

import "database/sql/driver"

type UserGender string

const (
	MaleUserGender   UserGender = "male"
	FemaleUserGender UserGender = "female"
)

// Scan implements the Scanner interface.
func (s *UserGender) Scan(value interface{}) error {
	*s = UserGender(string(value.([]uint8)))
	return nil
}

// Value implements the driver Valuer interface.
func (s UserGender) Value() (driver.Value, error) {
	return s.String(), nil
}

func (s UserGender) String() string {
	return string(s)
}

func (s UserGender) IsValid() bool {
	return s == MaleUserGender || s == FemaleUserGender
}
