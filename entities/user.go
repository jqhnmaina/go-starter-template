package entities

import (
	"time"

	"gopkg.in/guregu/null.v4"
)

type (
	PhoneNumber struct {
		CountryCode string `json:"country_code"`
		Number      string `json:"number"`
	}

	User struct {
		ID          int64       `json:"id"`
		Name        string      `json:"name"`
		DOB         time.Time   `json:"dob"`
		Gender      UserGender  `json:"gender"`
		Email       null.String `json:"email"`
		PhoneNumber PhoneNumber `json:"phone_number"`
		CreatedAt   time.Time   `json:"created_at"`
		UpdatedAt   null.Time   `json:"updated_at"`
	}
)
