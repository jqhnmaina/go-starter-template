package main

import (
	"log"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/repos"
	"bitbucket.org/jqhnmaina/go-starter-template/services"
	"bitbucket.org/jqhnmaina/go-starter-template/web"
)

func main() {

	userDB := db.NewPostgresDBManager()
	defer userDB.Close()

	err := userDB.Ping()
	if err != nil {
		log.Fatalf("error db ping: %v", err)
	}

	userService := services.NewUserService(repos.NewUserRepository())

	web.BuildRouter(
		userDB,
		userService,
	).Run()
}
