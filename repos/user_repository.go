package repos

import (
	"context"
	"database/sql"
	"time"

	"bitbucket.org/jqhnmaina/go-starter-template/db"
	"bitbucket.org/jqhnmaina/go-starter-template/entities"
)

const (
	selectUserSQL = "SELECT id, name, dob, gender, email, country_code, number, created_at, updated_at FROM users"

	findUserByEmail = selectUserSQL + " WHERE email = $1"

	findUserByID = selectUserSQL + " WHERE id = $1"

	findUserByPhoneNumber = selectUserSQL + " WHERE CONCAT(country_code, number) = $1"

	insertIntoUser = "INSERT INTO users(name, dob, gender, email, country_code, number, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id"
)

type (
	UserRepository interface {
		FindByEmail(context.Context, db.SQLOperations, string) (*entities.User, error)
		FindByID(context.Context, db.SQLOperations, int64) (*entities.User, error)
		FindByPhoneNumber(context.Context, db.SQLOperations, *entities.PhoneNumber) (*entities.User, error)
		Save(context.Context, db.SQLOperations, *entities.User) error
	}

	AppUserRepository struct {
	}
)

func NewUserRepository() UserRepository {
	return &AppUserRepository{}
}

func (r *AppUserRepository) FindByEmail(
	ctx context.Context,
	operations db.SQLOperations,
	email string,
) (*entities.User, error) {
	row := operations.QueryRowContext(ctx, findUserByEmail, email)

	return r.scanRowIntoUser(row)
}

func (r *AppUserRepository) FindByID(
	ctx context.Context,
	operations db.SQLOperations,
	userID int64,
) (*entities.User, error) {
	row := operations.QueryRowContext(ctx, findUserByID, userID)

	return r.scanRowIntoUser(row)
}

func (r *AppUserRepository) FindByPhoneNumber(
	ctx context.Context,
	operations db.SQLOperations,
	phoneNumber *entities.PhoneNumber,
) (*entities.User, error) {
	row := operations.QueryRowContext(ctx, findUserByPhoneNumber, phoneNumber.CountryCode+phoneNumber.Number)

	return r.scanRowIntoUser(row)
}

func (r *AppUserRepository) Save(
	ctx context.Context,
	operations db.SQLOperations,
	user *entities.User,
) error {
	if user.ID == 0 {
		user.CreatedAt = time.Now()

		res := operations.QueryRowContext(
			ctx,
			insertIntoUser,
			user.Name,
			user.DOB,
			user.Gender,
			user.Email,
			user.PhoneNumber.CountryCode,
			user.PhoneNumber.Number,
			user.CreatedAt,
		)

		err := res.Scan(&user.ID)
		return err
	}

	// Else we should update though for now no updates are needed

	return nil
}

func (r *AppUserRepository) scanRowIntoUser(
	row *sql.Row,
) (*entities.User, error) {

	user := entities.User{
		PhoneNumber: entities.PhoneNumber{},
	}

	err := row.Scan(
		&user.ID,
		&user.Name,
		&user.DOB,
		&user.Gender,
		&user.Email,
		&user.PhoneNumber.CountryCode,
		&user.PhoneNumber.Number,
		&user.CreatedAt,
		&user.UpdatedAt,
	)

	return &user, err
}
