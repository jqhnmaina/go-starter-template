# README #
To demo folder structure

### Development Requirements
* docker and docker-compose
* Be able to run `make` commands

### How do I get set up? ###

* clone the repo `git clone git@bitbucket.org:jqhnmaina/go-starter-template.git`
* cd in to project folder
* copy `.env.development.sample` to `env.development` 
* create the development container `make up`
* create users database `make createusers`
* migrate users table `make migrateusers`
* run `main.go`


**NB:** all environment variable necessary are listed in the `.env.development` file  
