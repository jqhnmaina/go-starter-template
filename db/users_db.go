package db

import (
	"context"
	"database/sql"
	"fmt"
	"os"

	// postgresql driver
	_ "github.com/lib/pq"
)

type SQLOperations interface {
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

type UserDB struct {
	*sql.DB
}

func NewPostgresDBManagerWithURL(databaseURL string) *UserDB {
	return newDBManagerWithParameters("postgres", databaseURL)
}

func NewPostgresDBManager() *UserDB {
	return newDBManagerWithParameters("postgres", os.Getenv("USERS_DATABASE_URL"))
}

func newDBManagerWithParameters(driverName string, databaseUrl string) *UserDB {

	if databaseUrl == "" {
		panic("database url is required")
	}

	db, err := sql.Open(driverName, databaseUrl)
	if err != nil {
		panic(fmt.Sprintf("sql.Open failed because err=[%v]", err))
	}

	return &UserDB{db}
}
