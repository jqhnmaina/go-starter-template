-- +goose Up
CREATE TYPE USER_GENDER AS ENUM ('male', 'female');

CREATE TABLE users
(
    id           BIGSERIAL PRIMARY KEY,
    name         VARCHAR(50) NOT NULL default '',
    dob          DATE        NOT NULL,
    gender       USER_GENDER NOT NULL,
    email        VARCHAR(100),
    country_code VARCHAR(7)  NOT NULL,
    number       VARCHAR(15) NOT NULL,
    created_at   TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp(),
    updated_at   TIMESTAMPTZ
);

CREATE UNIQUE INDEX users_country_code_number_uniq_idx ON users (country_code, number);
CREATE UNIQUE INDEX users_email_uniq_idx ON users (email) WHERE email IS NOT NULL;

-- +goose Down
DROP INDEX IF EXISTS users_country_code_number_uniq_idx, users_email_uniq_idx;
DROP TABLE users;
DROP TYPE USER_GENDER;
