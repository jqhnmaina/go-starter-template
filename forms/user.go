package forms

import (
	"time"

	"bitbucket.org/jqhnmaina/go-starter-template/entities"
)

type (
	CreatePhoneNumberForm struct {
		CountryCode string `binding:"required,min=1,max=3,numeric" json:"country_code"`
		Number      string `binding:"required,min=8,max=10,numeric" json:"number"`
	}

	CreateUserForm struct {
		Name        string                `binding:"required" json:"name"`
		DOB         time.Time             `binding:"required" json:"dob"`
		Gender      entities.UserGender   `binding:"required" json:"gender"`
		Email       string                `binding:"omitempty,email" json:"email"`
		PhoneNumber CreatePhoneNumberForm `binding:"required" json:"phone_number"`
	}
)
