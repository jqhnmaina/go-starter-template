include .env.development

createusers:
	docker-compose exec -T postgres psql -c "CREATE DATABASE users_development;"

migration:
	goose -dir db/migrations create $(name) sql

migrateusers:
	goose -dir 'db/migrations' postgres ${USERS_DATABASE_URL} up

rollbackusers:
	goose -dir 'db/migrations' postgres ${USERS_DATABASE_URL} down

up:
	docker-compose up
