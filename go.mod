module bitbucket.org/jqhnmaina/go-starter-template

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/lib/pq v1.9.0
	gopkg.in/guregu/null.v4 v4.0.0
)
